set -e

docker pull docker.med-logic.ru/medicine-call:latest
docker-compose stop
docker-compose up -d

docker rmi $(docker images -f "dangling=true" -q)
