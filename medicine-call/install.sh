set -e

echo "readonly" | docker login docker.med-logic.ru --username readonly --password-stdin

docker pull docker.med-logic.ru/medicine-call:latest

cd /root/medicine-config/medicine-stack
docker-compose exec postgres psql -c 'CREATE DATABASE medicine_call' --username=postgres

cat <<EOT >> /root/medicine-config/medicine-call/.env
EOT

cd /root/medicine-config/medicine-damask
docker-compose run --rm medicine-call python manage.py migrate
docker-compose run --rm medicine-call python manage.py post_install
docker-compose up -d
