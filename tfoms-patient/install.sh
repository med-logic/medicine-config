set -e

echo "readonly" | docker login docker.med-logic.ru --username readonly --password-stdin

docker pull docker.med-logic.ru/tfoms-patient:latest

read -p "Enter organization name: " ORGANIZATION_NAME

cat <<EOT >> /root/medicine-config/tfoms-patient/.env
SENTRY_SITE=$ORGANIZATION_NAME
EOT

cd /root/medicine-config/tfoms-patient && docker-compose up -d
