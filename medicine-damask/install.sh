set -e

echo "readonly" | docker login docker.med-logic.ru --username readonly --password-stdin

docker pull docker.med-logic.ru/medicine-damask:latest

cd /root/medicine-config/medicine-stack
docker-compose exec postgres psql -c 'CREATE DATABASE medicine_damask' --username=postgres

cat <<EOT >> /root/medicine-config/medicine-damask/.env
EOT

cd /root/medicine-config/medicine-damask
docker-compose run --rm medicine-damask python manage.py migrate
docker-compose run --rm medicine-damask python manage.py post_install
docker-compose up -d
