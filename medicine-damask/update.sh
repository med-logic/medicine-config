set -e

docker pull docker.med-logic.ru/medicine-damask:latest
docker-compose stop
docker-compose up -d

docker rmi $(docker images -f "dangling=true" -q)
