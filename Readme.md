
### Решение проблем

> Error saving credentials: error storing credentials - err: exit status 1, out: `Cannot autolaunch D-Bus without X11 $DISPLAY`

`> sudo apt remove golang-docker-credential-helpers`

> Git ignore rights

` > git config core.fileMode false`


### Ключи

#### Добавление ключика на удаленный сервер (root)

    ssh-copy-id -i ~/.ssh/medlogic_awx_rsa.pub -p 22 root@remote


#### Добавление ключика на удаленный сервер (no root)

    cat ~/.ssh/medlogic_awx_rsa.pub | ssh user@remote "sudo cat >> /root/.ssh/authorized_keys"


### Настройка сервера
> vim /etc/ssh/sshd_config

    PermitRootLogin without-password
    
    # Авторизация по паролю root
    PermitRootLogin yes

    PubkeyAuthentication yes

> service ssh restart

### Обратный ssh
на удаленном сервере

    ssh -R 40022:localhost:22 proxy_user@188.225.79.4

на доступном сервере

    ssh localhost -p 40022


### Crontab tasks
PATH=/usr/bin:/bin:/usr/local/bin
00 21 * * * /root/medicine-config/update_repository.sh
00 22 * * * /root/medicine-config/medicine/backup_database.sh
00 23 * * * /root/medicine-config/medicine/update.sh
00 22 * * * /root/medicine-config/medicine-remd/backup_database.sh
30 23 * * * /root/medicine-config/medicine-remd/update.sh
00 00 * * * /root/medicine-config/scripts/docker-cleanup.sh

00 21 * * * /root/medicine-config/update_repository.sh
00 22 * * * /root/medicine-config/medicine/backup_database.sh
00 23 * * * /root/medicine-config/medicine/update.sh
00 00 * * * /root/medicine-config/scripts/docker-cleanup.sh

* * * * * /root/medicine-config/scripts/crontab-check.sh >> /var/log/crontab-check.log 2>&1

sudo timedatectl set-timezone Asia/Vladivostok

# /etc/hosts
> sudo vim /etc/hosts  
89.223.70.16 docker.med-logic.ru git.med-logic.ru

# change git url

git remote set-url origin https://git.med-logic.ru/ilya.muhortov/medicine-config.git && git pull

### Перенос данных
scp -r /root/medicine-config/medicine-lis-receiver/supervisor_confs root@hostname:/root/medicine-config/medicine-lis-receiver/supervisor_confs
scp -r /var/lib/docker/volumes/medicine_medicine-public-data/_data root@hostname:/var/lib/docker/volumes/medicine_medicine-public-data

# Разное

curl -iv --tlsv1.2 https://docker.med-logic.ru/
