set -e

echo "readonly" | docker login docker.med-logic.ru --username readonly --password-stdin

docker pull docker.med-logic.ru/medicine-lis:latest

cd /root/medicine-config/medicine-stack
docker-compose exec postgres psql -c 'CREATE DATABASE medicine_lis' --username=postgres

cat <<EOT >> /root/medicine-config/medicine-lis/.env
EOT

cd /root/medicine-config/medicine-lis
docker-compose run --rm medicine-lis python manage.py migrate
docker-compose run --rm medicine-lis python manage.py post_install
docker-compose up -d
