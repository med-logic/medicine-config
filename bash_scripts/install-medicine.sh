set -e

echo "readonly" | docker login docker.med-logic.ru --username readonly --password-stdin

docker pull docker.med-logic.ru/medicine:latest

unzip /root/medicine-config/backups/medicine.sql.zip -d /root/medicine-config/volumes/files/

docker exec -i medicine-stack-postgres psql -U postgres -d medicine -f /files/medicine.sql

read -p "Enter organization name: " ORGANIZATION_NAME
read -p "Enter organization id: " ORGANIZATION_ID

SECRET_KEY=$(openssl rand -base64 32)

cat <<EOT >> /root/medicine-config/medicine/.env

SECRET_KEY=$SECRET_KEY

SENTRY_SITE=$ORGANIZATION_NAME

ORGANIZATION_ID=$ORGANIZATION_ID

EOT

cd /root/medicine-config/medicine
docker-compose run --rm app python manage.py migrate --noinput
docker-compose up -d
