set -e

apt install --assume-yes autossh
ssh-keygen -f ~/.ssh/proxy-server -N ''
ssh-copy-id -i ~/.ssh/proxy-server.pub -p 22 proxy_user@188.225.79.4

read -p "Enter port for proxy server: " proxy_port

cat <<EOT >> /etc/systemd/system/remote-autossh.service
[Unit]
Description=AutoSSH service for remote tunnel
After=network-online.target
Wants=network-online.target systemd-networkd-wait-online.service

[Service]
User=root
Restart=on-failure
RestartSec=1min
ExecStart=/usr/bin/autossh -N -R $proxy_port:localhost:22 -i /root/.ssh/proxy-server proxy_user@188.225.79.4

[Install]
WantedBy=multi-user.target
EOT

systemctl daemon-reload && systemctl start remote-autossh
systemctl enable remote-autossh.service

cat <<EOT >> /root/.ssh/authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCyPQET8KBriZjuB1PiFtBYtGZJphivZWHT4JtGJNDLpwxyUgK6ZzVrF5MvJ27eQBoM1pMxzVl3XWwgxo1BQkCGIsqERwZ5/onfsWTH/COyuZeNmL5NF3NYLjoSF9a0zwHpEQxeLGf+3qQrEfTOZKuPelc/8kmQjYDPo97Ow3d+6LA9Mlhe2S7pYsvVgGy+XjGnegRZddAZu6vnDCGrDbfSgeW0KTQCLhGmtQao99SRxUOlqHfU13x9IZAHldikbhWbXxgc1+1CH/1iYbRqnVdgAAyJzguFIkAJGKzEnLiwHVgJF1Gf85FItF8KaNHCIv5T7RPBvFUIN/gw3QO/jTxJ root@1041123-muhortov1.tmweb.ru
EOT
