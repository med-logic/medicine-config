set -e

echo "readonly" | docker login docker.med-logic.ru --username readonly --password-stdin

docker pull docker.med-logic.ru/medicine-remd:latest

read -r -p "Create database? [y/N] " response
if [[ $prompt =~ [yY](es)* ]]
then
  docker exec medicine-stack-postgres psql -c 'CREATE DATABASE medicine_remd' --username=postgres
fi

read -p "Enter organization name: " ORGANIZATION_NAME

SECRET_KEY=$(openssl rand -base64 32)

cat <<EOT >> /root/medicine-config/medicine-remd/.env

SECRET_KEY=$SECRET_KEY

SENTRY_ENABLED='True'

SENTRY_SERVER_NAME=$ORGANIZATION_NAME

EOT

cd /root/medicine-config/medicine-remd
docker-compose run --rm medicine-remd python manage.py migrate
docker-compose run --rm medicine-remd python manage.py post_install
docker-compose up -d
