set -e

cd /root/medicine-config/thirdparty-apps
docker-compose pull
docker-compose build
docker-compose up -d
