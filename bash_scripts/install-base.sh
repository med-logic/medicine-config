
set -e

NETWORK_NAME=medicine-network
if [ -z $(docker network ls --filter name=^${NETWORK_NAME}$ --format="{{ .Name }}") ] ; then
  docker network create ${NETWORK_NAME} ;
fi

cd /root/medicine-config/medicine-stack
docker-compose up -d
sleep 10
docker-compose exec postgres psql -c 'CREATE DATABASE medicine' --username=postgres
docker-compose exec postgres psql -d medicine -c "CREATE ROLE readonly; GRANT USAGE ON SCHEMA public TO readonly; GRANT SELECT ON ALL TABLES IN SCHEMA public TO readonly; ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO readonly; CREATE USER readonly_user WITH PASSWORD 'readonly_password'; GRANT readonly TO readonly_user; ALTER ROLE readonly WITH LOGIN;" --username=postgres
