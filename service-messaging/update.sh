
set -e

docker pull docker.med-logic.ru/medicine-ntf:latest
docker-compose stop
docker-compose up -d
docker-compose run --rm service-messaging python manage.py migrate