set -e

echo "readonly" | docker login docker.med-logic.ru --username readonly --password-stdin

docker pull docker.med-logic.ru/medicine-ntf:latest

read -r -p "Create database? [y/N] " response
if [[ $prompt =~ [yY](es)* ]]
then
  cd /root/medicine-config/medicine-stack
  docker-compose exec postgres psql -c 'CREATE DATABASE service_messaging' --username=postgres
fi

read -p "Enter organization name: " ORGANIZATION_NAME

SECRET_KEY=$(openssl rand -base64 32)

cat <<EOT >> /root/medicine-config/service-messaging/.env

SECRET_KEY=$SECRET_KEY

SENTRY_ENABLED='True'

SENTRY_SERVER_NAME=$ORGANIZATION_NAME

EOT
