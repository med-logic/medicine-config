set -e

echo "readonly" | docker login docker.med-logic.ru --username readonly --password-stdin

docker pull docker.med-logic.ru/medicine-import:latest

cd /root/medicine-config/medicine-stack
docker-compose exec postgres psql -c 'CREATE DATABASE medicine_import' --username=postgres

cd /root/medicine-config/medicine-import
docker-compose run --rm medicine-import python manage.py post_install
docker-compose up -d
