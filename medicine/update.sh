#!/bin/sh

set -e

echo "readonly" | docker login docker.med-logic.ru --username readonly --password-stdin
docker pull docker.med-logic.ru/medicine:latest

(cd /root/medicine-config/medicine &&
  docker-compose stop &&
  docker-compose up -d
)
