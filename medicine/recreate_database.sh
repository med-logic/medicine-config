set -e

docker stop medicine-app medicine-tasks
docker exec medicine-stack-postgres psql -c 'DROP DATABASE medicine' --username=postgres
docker exec medicine-stack-postgres psql -c 'CREATE DATABASE medicine' --username=postgres
