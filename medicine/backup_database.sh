#!/bin/sh

set -e
export LC_ALL=C

mkdir -p /root/medicine-backups/
cd /root/medicine-backups/

weekday_number=$(date +%u)
weekday=$(date +%A | tr '[:upper:]' '[:lower:]')
backup_filename="medicine_${weekday_number}_${weekday}.sql"
archive_filename="medicine_${weekday_number}_${weekday}.tar.gz"
docker exec -i medicine-stack-postgres pg_dump --no-owner -U postgres -F p medicine > "${backup_filename}" \
  --exclude-table-data=riisz_logging \
  --exclude-table-data=reversion_*

if test -f "${archive_filename}"; then
  rm "${archive_filename}"
fi
tar -czf "${archive_filename}" "${backup_filename}"
rm "${backup_filename}"

