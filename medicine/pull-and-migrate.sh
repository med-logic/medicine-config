#!/bin/sh

set -e

echo "readonly" | docker login docker.med-logic.ru --username readonly --password-stdin
docker pull docker.med-logic.ru/medicine:latest
docker run --rm --env-file .env --network medicine-network --name medicine-apply-migrations docker.med-logic.ru/medicine:latest python manage.py migrate --noinput
