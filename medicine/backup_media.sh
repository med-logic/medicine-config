#!/bin/sh

set -e

mkdir -p /root/medicine-backups/
cd /root/medicine-backups/
archive_filename="medicine_media.zip"

if test -f "${archive_filename}"; then
  rm "${archive_filename}"
fi

zip -r "${archive_filename}" /var/lib/docker/volumes/medicine_medicine-public-data/_data/media
