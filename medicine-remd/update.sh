#!/bin/sh

set -e

echo "readonly" | docker login docker.med-logic.ru --username readonly --password-stdin
docker pull docker.med-logic.ru/medicine-remd:latest

(cd /root/medicine-config/medicine-remd &&
  docker-compose stop &&
  docker-compose up -d
)
